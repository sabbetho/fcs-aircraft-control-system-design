% Determine where your m-file's folder is.
folder = fileparts(which(mfilename)); 
% Add that folder plus all subfolders to the path.
addpath(genpath(folder));
clear folder
Airframe;
Aerodynamic;
Powertrain;
AerodynamicSurfaces;
Initial;
Trim;

%% Calculation for stall speed etc.:
% We can calculate the lift during cruise because we know that lift =
% weight. 
cl0 = plane.Aerodynamic.CL0;
% We suppose 10degrees AOA is maximum, and therefore CL @ this AOA = CLmax
cl_at_alpha = cl0 + deg2rad(10)*2*pi;
height_for_density = 15; % final flare maneuvre altitude (may be lower but will result in around the same)
density_value = 1.225 * exp(-height_for_density/9297);
lift_in_cruise = 3.9175 * 10000; % 70m/s at 1000m
S = 19.478 %(plane.Airframe.S)

v_stall = power((2*lift_in_cruise)/(density_value*S*cl_at_alpha), 0.5)

minimum_control_speed_landing = 120/3.6; % See EASA document: 33 m/s
%http://www.let.cz/en/l410ng

flight_path_constant = 800/(3*1.3*48);  % 800m long runway, as requested.

desired_glide_angle = -3;% IN DEGREES (as requested in the requirements)
h_flare_initial = 21;

Long

Lat

close all;