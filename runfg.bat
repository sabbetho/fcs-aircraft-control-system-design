G:

cd G:\Program Files (x86)\FlightGear 2020.3

SET FG_ROOT=G:\Program Files (x86)\FlightGear 2020.3\data

START .\\bin\fgfs.exe --fdm=null --native-fdm=socket,in,30,localhost,5502,udp    --aircraft=SenecaII --prop:/engines/engine0/running=true --fog-fastest --disable-clouds --prop:/engines/engine[0]/running=true --enable-clouds3d --enable-auto-coordination --start-date-lat=2022:12:05:09:00:00 --disable-sound --airport=EBBR --runway=25L --offset-distance=0 --offset-azimuth=0