%% Longitunal model
load('LinSys.mat')
sysLong = linsys1([1,3,5,10,12],[3,5,6,7,8]); 
% Here is the description of how the linear system is defined:
% First array is the output, second array is the input
% Output: [1 Airspeed, 2 Roll Angle, 3 Pitch Angle, Body Rotational Rates (4 Roll, 5 Pitch, 6 Yaw), NED (7, 8, 9), 10 Altitude , 11 Heading, 12 AoA, 13 AoS(Angle of Sideslip (beta))]
% Input: [ 1 Aileron L Deflection, 2 Aileron R Deflection, 3 Elevator Deflection, 4 Rudder Deflection, 5 & 6 Engine RPM, 7 & 8 Propeller Pitch]
s= tf("s");

%% #1 Pitch Damper &  #2 Pitch Attitude Hold 
% Pitch Damper & Attitude Hold Param
K_PitchDamper = 1.15;
P_PitchAttitude = 9.1908;
I_PitchAttitude = 1.23107;
D_PitchAttitude = -0.48296;
N_PitchAttitude = 6.7654;


SysLongDamper = feedback(sysLong,K_PitchDamper,1,3);% Elevator deflection / Body Rotational Rates (3, 5 ) in full form
PID_PitchAttitude= P_PitchAttitude + I_PitchAttitude/s + D_PitchAttitude* N_PitchAttitude/ (1 + N_PitchAttitude/s) ; 
PID_PitchAttitude_MIMO = [PID_PitchAttitude,0,0,0,0;
           zeros(4,1),eye(4)];
PID_SysLongDamper = SysLongDamper*PID_PitchAttitude_MIMO; % This is the open loop transfer function
figure()
margin(PID_SysLongDamper(2,1))

Sys_Long_Damper_PitchAttHold = feedback(PID_SysLongDamper,1,1,2); %closed loop system of pitch reference to elevator deflection
% Analysis
figure()
step(Sys_Long_Damper_PitchAttHold(2,1),5) % step response of the elevator deflection to pitch angle for 5 s
% step(Sys_Long_Damper_PitchAttHold(4,1)*deg2rad(1),5) 

%% FeedForward Controller of Altitude Hold
% For the feedforward controller, the inverse of the transfer function of
% pitch rate to altitude is needed. 
Sys_PitchRef_Altitude =Sys_Long_Damper_PitchAttHold(4,1); % Get the Steady State system Altitude (4) to Pitch Reference(1) [Note that Input 1 has changed from Elevator deflection to Pitch Reference in that new combined system!]
% TF_Sys_PitchRef_ElevDefl= tf(Sys_PitchRef_ElevDefl);
% NUM_TF_Sys_PitchRef_ElevDefl = cell2mat(TF_Sys_PitchRef_ElevDefl.Numerator);
% DEN_TF_Sys_PitchRef_ElevDefl = cell2mat(TF_Sys_PitchRef_ElevDefl.Denominator);

Pole_Sys_PitchRef_ElevDefl = pole(minreal(Sys_PitchRef_Altitude));
Zero_Sys_PitchRef_ElevDefl = zero(minreal(Sys_PitchRef_Altitude));
Approx_Sys_PitchRef_ElevDefl= zpk(Zero_Sys_PitchRef_ElevDefl(2:end),Pole_Sys_PitchRef_ElevDefl(5:end),0.0951); %dcgain(Sys_PitchRef_ElevDefl) check to see if this resembles the dynamics
[NUM_Approx_Sys_PitchRef_ElevDefl, DEN_Approx_Sys_PitchRef_ElevDefl] = zp2tf(Approx_Sys_PitchRef_ElevDefl.Z{:},Approx_Sys_PitchRef_ElevDefl.P{:},Approx_Sys_PitchRef_ElevDefl.K);
% Inverse generation method 1:
Inverse_Sys_PitchRef_ElevDefl_TF = tf(inv(Approx_Sys_PitchRef_ElevDefl));
NUM_Inverse_Sys_PitchRef_ElevDefl_TF = cell2mat(Inverse_Sys_PitchRef_ElevDefl_TF.Numerator);
DEN_Inverse_Sys_PitchRef_ElevDefl_TF = cell2mat(Inverse_Sys_PitchRef_ElevDefl_TF.Denominator);
%Inverse generation method 2
Inverse_Sys_PitchRef_ElevDefl = zpk(Pole_Sys_PitchRef_ElevDefl(5:end),Zero_Sys_PitchRef_ElevDefl(2:end),1/0.0951); % dcgain(Sys_PitchRef_ElevDefl) This can be used as feed forward controller
[NUM_Inverse_Sys_PitchRef_ElevDefl, DEN_Inverse_Sys_PitchRef_ElevDefl] = zp2tf(Inverse_Sys_PitchRef_ElevDefl.Z{:},Inverse_Sys_PitchRef_ElevDefl.P{:},Inverse_Sys_PitchRef_ElevDefl.K);
% step(Sys_PitchRef_Altitude)
% hold on
% step(Approx_Sys_PitchRef_ElevDefl)


%% #3 Altitude Hold
% Altitude Hold Param
P_AltitudeHold = 0.010239; 
I_AltitudeHold = 0.0000034135; 
PI_AltitudeHold = tf([P_AltitudeHold,I_AltitudeHold],[1,0]);
PI_AltitudeHold_MIMO = [PI_AltitudeHold,0,0,0,0;
           zeros(4,1),eye(4)];
Sys_Altitude_OL = Sys_Long_Damper_PitchAttHold*PI_AltitudeHold_MIMO;
Sys_Long_Damper_PitchAttHold_AltitudeHold = feedback(Sys_Altitude_OL, 1, 1, 4);
% From here follows the analysis of the full Altitude hold system
figure()
step(Sys_Long_Damper_PitchAttHold_AltitudeHold(4,1)) % Step Response of the Altitude Hold --> 1m Altitude change as input and the plot output is the altitude response
title('Step Resp. Altitude Hold CL')
% Bode plots for stability analysis
step(Sys_Long_Damper_PitchAttHold_AltitudeHold(1,4)) % Step Response of the Altitude Hold --> 1m Altitude change as input and the plot output is the altitude response
figure()
margin(Sys_Altitude_OL(4,1)) %
title('Stab. Margins Altitude Hold')


%% Mach Hold Param
P_MachHold = 73.226;
I_MachHold = 0.7252;
PI_MachHold = tf([P_MachHold,I_MachHold],[1,0]);
PI_MachHold_MIMO = [1,0,0,0;
           0,1,0,0;
           0,0,1,0;
           0,0,0,PI_MachHold;
           0,0,0,PI_MachHold];
Sys_MachHold_OL = Sys_Long_Damper_PitchAttHold_AltitudeHold*PI_MachHold_MIMO;
Sys_Long_Damper_PitchAttHold_AltitudeHold = feedback(Sys_MachHold_OL, 1, 4, 1);

% Mach Hold Controller Analysis
figure()
step(Sys_Long_Damper_PitchAttHold_AltitudeHold(1,4)) % Step Response of the Mach Hold --> 1m Airspeed change as input and the plot output is the Airspeed response
title('Step Resp. Mach Hold CL')
figure()
margin(Sys_MachHold_OL(1,4)) %Bode plot is for the open loop system and gives information about the frequency behaviour of the closed loop system.
title('Stab. Margins Altitude Hold')



%% Mach hold FeedForward (Tutor)
p2a = Sys_Long_Damper_PitchAttHold(1,end)+Sys_Long_Damper_PitchAttHold(1,end-1); % Sys_Long_Damper_PitchAttHold(1,5) + Sys_Long_Damper_PitchAttHold(1,4) ### TF from  Engine Pitch L&R (4/5) to Airspeed(1)
pole_p2a = pole(minreal(p2a)); % both controllable and observable (see and influence) : changes A
zero_p2a = zero(minreal(p2a));
ff = zpk(pole_p2a,[zero_p2a;-0.1],dcgain(p2a));
% step(p2a*ff);
[b,a] = zp2tf(ff.Z{:},ff.P{:},ff.K);



%% Stability Analysis Longitudal Dynamics
% % Closed Loop System TF with Pitch Damper:
% G_ED_PA_CL = (G_ED_PA* G_C_PitchAttitude)/(1 + G_ED_BRR*K_PitchDamper + G_ED_PA* G_C_PitchAttitude);
% % Open Loop:
% G_ED_PA_OL = (G_ED_PA* G_C_PitchAttitude)/(1 + G_ED_BRR*K_PitchDamper);
% figure()
% margin(G_ED_PA_OL)
% figure()
% step(G_ED_PA_CL)
% hold on 
% step(feedback(G_ED_PA_OL,1))% Why is there a mismatch? 
% figure()
% bode(G_ED_PA_CL)% from this we can find the stability margins
% pole(minreal(G_ED_PA_CL))

%% Toto Version of Feedforward Control (Old)
G_EnginePitch1_Airsp = tf(linsys1(1,7)); % Engine Pitch 1 to Airspeed 
G_EnginePitch2_Airsp = tf(linsys1(1,8)); % Engine Pitch 2 to Airspeed 
G_EP_Airsp_FB = feedback(tf(linsys1(1,7)),1); % Engine Pitch to Airspeed 

Pole_G_EP_Airsp = pole(minreal(G_EnginePitch1_Airsp));
Zero_G_EP_Airsp = zero(minreal(G_EnginePitch1_Airsp));
% step(G_EnginePitch1_Airsp)
% step(G_EnginePitch2_Airsp)
% step(G_EnginePitch1_Airsp+G_EnginePitch2_Airsp)
% 
% step(G_EP_Airsp_FB)


% Approximating TF as first order system:
TF_Approx = 0.54*(s+0.1)/22.9/ (s+1/229); % Steady state value at 0.023 and T_s at 299 s
TF_Approx_FB = 0.35*(s+0.1)/17.8/ (s+1/178); % Steady state value at 0.023 and T_s at 299 s
Pole_TF_Approx = pole(minreal(TF_Approx));
Zero_TF_Approx = zero(minreal(TF_Approx));
figure()
step(TF_Approx)
hold on
step(G_EnginePitch1_Airsp)
%Inverting that Transfer Function
FeedForward_MachHold = zpk(Pole_TF_Approx,Zero_TF_Approx,dcgain(TF_Approx));
[NUM_FeedForward_MachHold ,DEN_FeedForward_MachHold] = zp2tf(FeedForward_MachHold.Z{:},FeedForward_MachHold.P{:},FeedForward_MachHold.K);
step(FeedForward_MachHold)