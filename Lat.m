%% Lateral model
close all
load('LinSys.mat')
sysLat = linsys1([2,4,6,11,13],[1,2,4]); %% C and B
% Here is the description of how the linear system is defined:
% First array is the output, second array is the input
% Output: [1 Airspeed, 2 Roll Angle, 3 Pitch Angle, Body Rotational Rates (4 Roll, 5 Pitch, 6 Yaw), NED (7, 8, 9), 10 Altitude , 11 Heading, 12 AoA, 13 AoS(Angle of Sideslip (beta))]
% Input: [ 1 Aileron L Deflection, 2 Aileron R Deflection, 3 Elevator Deflection, 4 Rudder Deflection, 5 & 6 Engine RPM, 7 & 8 Propeller Pitch]
s= tf("s");
% Here is the description of how the linear system is defined:
%% Roll damper
P_RollDamper = 3;%2;
size(sysLat)
% For the mapping: sysLat: State-space model with 5 outputs, 3 inputs, and 19 states.
sysLat_adjusted = sysLat*[-1,0;1,0;0,1]; % State-space model with 5 outputs, 2 inputs, and 19 states.
figure('Name','Bode plot sysLat_adjusted')
margin(sysLat_adjusted(2,1))
sysLatDamper = feedback(sysLat_adjusted,P_RollDamper,1,2); % from adjusted system their inputs
figure('Name', 'Step for sysLatDamper')
step(sysLatDamper(2,1)) % step response of the roll reference to the aileron deflection for 5 s

%% Roll attitude hold (includes the damper)
P_RollAttitude = 4.047; % P_RollAttitude*0.5
I_RollAttitude = 0.31954048; % I_RollAttitude*0.05
D_RollAttitude = 0.36624; % D_RollAttitude*0.2
N_RollAttitude = 3.01924956197251;
PID_RollAttitude = P_RollAttitude + I_RollAttitude/s + D_RollAttitude* N_RollAttitude/ (1 + N_RollAttitude/s) ; %tf([D_PitchAttitude, P_PitchAttitude,I_PitchAttitude],[1,0]);
PIDsysLatHold = sysLatDamper * [PID_RollAttitude,0;0,1];
figure('Name','Bode plot PIDsysLatHold')
margin(PIDsysLatHold(1,1))
sysRollAttHold = feedback(PIDsysLatHold,1,1,1); % from adjusted system their inputs
%Bode plot of the Roll Attitude Hold:
figure('Name', 'Step for sysRollAttHold')
step(sysRollAttHold(1,1))
%% Heading hold (includes RAH and RD)
P_HeadingHold = 5; % 7*0,6
I_HeadingHold = 0.105; % 1*0.2
D_HeadingHold = 0;
N_HeadingHold = 0;
PID_HeadingHold = P_HeadingHold + I_HeadingHold/s + D_HeadingHold* N_HeadingHold/ (1 + N_HeadingHold/s) ;
PsysHeadingHold = sysRollAttHold * [PID_HeadingHold,0;0,1];
figure('Name','Bode plot PsysHeadingHold')
margin(PsysHeadingHold(4,1))
sysHeadingHold = feedback(PsysHeadingHold, 1, 1,4); % from adjusted system their inputs (heading (number 2: 4 of 6 outputs) to roll reference (1 of 2 inputs)
figure('Name','Step for HeadingHold')
step(sysHeadingHold(4,1))
%% Yaw damper
P_YawDamper = 1;
tau = 5;% washout filter timeconstant. As can be seen from the root loci, if the time constant is not
%large enough, very little increase in the damping ratio can be obtained. If the
%time constant is larger, then considerable improvement of the damping ratio
%can be obtained as the rate gyro sensitivity is increased.
HighPassFilter_den = [tau 0];
HighPassFilter_num = [tau 1];
HighPassFilter = tau*s / (tau*s+1); % must be a copy paste of the upper num and den parts!
Elements = P_YawDamper*HighPassFilter; 
sysYawDamper_adjusted = sysHeadingHold * [1,0;0,Elements];
figure('Name','Bode plot sysYawDamper_adjusted')
margin(sysYawDamper_adjusted(3,2))
sysYawDamper = feedback(sysYawDamper_adjusted,1,2,3); % from adjusted system their inputs (yaw rate (number 6) to rudder reference (2 of 2 inputs)
figure('Name','Step for sysYawDamper')
step(sysYawDamper(3,2))

%% AoS "hold" (hold on 0)
P_AoSHold =2.1229;%-7.0497;  %15.6162;%-10.410
I_AoSHold = 10.8284;%-6.0045;
%I_AoSHold = %4.22703;%-70.4505
D_AoSHold = 0.042838;
N_AoSHold = 220.3747;
PID_AoSHold = P_AoSHold + I_AoSHold/s; %tf([D_PitchAttitude, P_PitchAttitude,I_PitchAttitude],[1,0]);
PID_AoSHold_adjusted = sysYawDamper * [1,0;0,PID_AoSHold]; % just one because the PID is now NOT summed on the loop, instead it's (negative, obviosuly) feedback!!
figure('Name','Bode plot PID_AoSHold_adjusted')
margin(PID_AoSHold_adjusted(5,2))
PIDsysAoSHold = feedback(PID_AoSHold_adjusted,1,2,5); % 
figure('Name','Step for PIDsysAoSHold')
step(PIDsysAoSHold(5,2))